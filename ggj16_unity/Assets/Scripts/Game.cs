﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{
    public static Game Instance
    {
        get
        {
            return s_instance;
        }
    }

    static Game s_instance;
    
    [SerializeField]
    GameObject m_systemsPrefab;
    
    GameObject m_systems;

    public InputManager Input { get; private set; }

    public int Winner { get; set; }

    void Awake ()
    {
        if (s_instance == null)
        {
            s_instance = this;
            DontDestroyOnLoad(this.gameObject);

            m_systems = Instantiate(m_systemsPrefab);
            m_systems.transform.parent = transform;

            Input = m_systems.GetComponent<InputManager>();

            m_state = (State)Application.loadedLevel;
        }
        else
        {
            Destroy(this.gameObject);
        }
	}

    public enum State
    {
        SPLASH,
        GAME,
        WIN
    }

    State m_state;

    public void SetState(State _state)
    {
        if (m_state != _state)
        {
            m_state = _state;
            StartCoroutine(LoadLevelSequence(m_state));
        }
    }

    [ContextMenu("splash")]
    void DebugLoadSplash()
    {
        SetState(State.SPLASH);
    }

    [ContextMenu("game")]
    void DebugLoadGame()
    {
        SetState(State.GAME);
    }

    [ContextMenu("win")]
    void DebugLoadWin()
    {
        SetState(State.WIN);
    }

    IEnumerator LoadLevelSequence(State _state)
    {
        Debug.Log("STARTING");

        yield return new WaitForSeconds(0.3f);
        Debug.Log("LOADING " + (int)_state);

        Application.LoadLevel((int)_state);

        yield return new WaitForSeconds(0.3f);

        Debug.Log("DONE");
    }
}
