﻿using UnityEngine;
using System.Collections;

public class ResizeQuadAfterCamera : MonoBehaviour
{
    [SerializeField]
    GameObject m_quad;

    [SerializeField]
    Camera m_camera;

	void Update ()
    {
        Rescale();
    }

    [ContextMenu("Rescale")]
    void Rescale()
    {
        if (m_camera.aspect > 1.0f)
        {
            //Adjust to get fixed amount of pixels per cell
            float adj = Mathf.Floor(1.0f + m_camera.pixelWidth / 256.0f) / (m_camera.pixelWidth / 256.0f);
            m_quad.transform.localScale = Vector3.one * m_camera.orthographicSize * 2.0f * m_camera.aspect * adj;
        }
        else
        {
            //Adjust to get fixed amount of pixels per cell
            float adj = Mathf.Floor(1.0f + m_camera.pixelHeight / 256.0f) / (m_camera.pixelHeight / 256.0f);
            m_quad.transform.localScale = Vector3.one * m_camera.orthographicSize * 2.0f * adj;
        }
    }
}
